import React, {Component} from 'react';
import { TodoListContainer } from "./components";
import store from "./redux/store";
import {Provider} from "react-redux";


class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <div className="App">
                    <h3>Todos</h3>
                    <TodoListContainer />
                </div>
            </Provider>
        );
    }
}

export default App;
