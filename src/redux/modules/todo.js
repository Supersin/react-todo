import axios from 'axios';

const ADD_TODO_SUCCESS = 'react-todo-new/todo/ADD_TODO_SUCCESS';
const TOGGLE_TODO_SUCCESS = 'react-todo-new/todo/TOGGLE_TODO';

const initialState = [];

export default function reducer(state = initialState, action = {}) {
    switch (action.type) {
        case ADD_TODO_SUCCESS:
            return [
                ...state,
                { id: action.payload.id, text: action.payload.text, completed: false }
            ];
        case TOGGLE_TODO_SUCCESS:
            return state.map(todo =>
                todo.id === action.payload.id
                    ? {...todo, completed: !todo.completed}
                    : todo
            );
        default:
            return state;
    }
}

export const addTodo = (text, id) => ({
    type: ADD_TODO_SUCCESS,
    payload: {
        id, text
    }
});

export function createTodo(text) {
    return dispatch => {
        return axios.post(
            'http://localhost:6543/api/todo/new',
            text
        ).then(response => {
            const todoId = response.data;
            return dispatch(addTodo(text, todoId))
        }).catch(err => {
            return Promise.reject(err);
        });
    };
}

export const toggleTodo = id => ({
    type: TOGGLE_TODO_SUCCESS,
    payload: {
        id
    }
});

export function updateTodo(id) {
    return dispatch => {
        return axios.post(
            `http://localhost:6543/api/todo/${id}/toggle`,
        ).then(() => {
            return dispatch(toggleTodo(id))
        }).catch(err => {
            return Promise.reject(err);
        })
    }
}
