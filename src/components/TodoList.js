import React from 'react'
import TodoItem from './TodoItem'

const TodoList = ({ onUpdateTodo, todos }) => (
    <ul style={{'listStyleType': 'none'}}>
        {todos.map(todo =>
            <TodoItem
                key={todo.id}
                id={todo.id}
                text={todo.text}
                onToggle={onUpdateTodo}
                completed={todo.completed}
            />
        )}
    </ul>
);

export default TodoList;
