import React from 'react'

const TodoItem = ({ onToggle, completed, text, id}) => {
    return(
        <li
            style={{
                textDecoration: completed ? 'line-through' : 'none'
            }}
        >
            <div>
                <input type="checkbox" onChange={() => onToggle(id)} />
                <span>{text}</span>
            </div>
        </li>
    );
};

export default TodoItem
