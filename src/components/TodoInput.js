import React, { Component } from 'react'

class TodoInput extends  Component {
    state = {
        todoText: ''
    };

    onChangeTodoText = e => {
        const todoText = e.target.value.trim();

        this.setState({ todoText });
    };

    onCreateTodoHandler = e => {
        e.preventDefault();

        const { todoText } = this.state;
        this.props.onCreateTodo(todoText);

        this.setState({ todoText: '' })
    };

    render() {
        const { todoText } = this.state;

        return (
            <div>
                <form onSubmit={this.onCreateTodoHandler}>
                    <input
                        value={todoText}
                        placeholder="Add todo"
                        onChange={this.onChangeTodoText}
                    />
                </form>
            </div>
        )

    }
}

export default TodoInput;
