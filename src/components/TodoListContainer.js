import React, { Component } from 'react';
import TodoInput from "./TodoInput";
import TodoList from "./TodoList";
import { connect } from 'react-redux'
import { createTodo, updateTodo } from '../redux/modules/todo';

class TodoListContainer extends Component {
    onCreateTodoHandler = text => {
        this.props.createTodo(text)
    };

    onUpdateTodoHandler = id => {
        this.props.updateTodo(id)
    };

    render() {
        const { todos } = this.props;
        return (
            <div>
                <TodoInput onCreateTodo={this.onCreateTodoHandler} />
                <TodoList onUpdateTodo={this.onUpdateTodoHandler} todos={todos} />
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        todos: state
    };
};

export default connect(mapStateToProps, {
    createTodo,
    updateTodo
})(TodoListContainer);